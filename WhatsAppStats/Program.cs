﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WhatsAppStats
{
    class Program
    {
        static Regex messageRegex = new Regex(@"\d{2}/\d{2}/\d{4}, \d{2}:\d{2} - ([^:]+): ", RegexOptions.Compiled);
        static Regex firstWordRegex = new Regex(@"\s*(\S+)", RegexOptions.Compiled);
        static char[] punctuationChars = { '`', '¬', '!', '"', '£', '$', '%', '^', '&', '*', '(', ')', '-', '=', '_', '+', '[', ']', '{', '}', ';', '\'', '#', ':', '@', '~', ',', '.', '/', '<', '>', '?', '\\', '|' };
        const int WORDS_PER_PERSON = 20;

        static void ExtractMessage(string file, int index, int length, Dictionary<string, Dictionary<string, int>> firstWordCounts, Dictionary<string, int> totalMessages, string currentName)
        {
            string s = file.Substring(index, length);
            if (s == "<Media omitted>\n")
                return;

            Match m = firstWordRegex.Match(s);
            if (m.Success)
            {
                string trial;
                string firstWord = m.Groups[1].Value;
                firstWord = firstWord.Replace('‘', '\'');
                firstWord = firstWord.Replace('’', '\'');
                firstWord = firstWord.Replace('“', '"');
                firstWord = firstWord.Replace('”', '"');
                trial = firstWord.TrimEnd(punctuationChars);
                if (trial.Length > 0)
                    firstWord = trial;
                trial = firstWord.TrimStart(punctuationChars);
                if (trial.Length > 0)
                    firstWord = trial;
                if (!firstWordCounts.TryGetValue(currentName, out Dictionary<string, int> counts))
                {
                    counts = new Dictionary<string, int>();
                    firstWordCounts[currentName] = counts;
                }
                counts.TryGetValue(firstWord, out int count);
                counts[firstWord] = ++count;
                if (!totalMessages.TryGetValue(currentName, out int total))
                    total = 0;
                totalMessages[currentName] = ++total;
            }
        }
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(args[0], Encoding.UTF8);
            string file = sr.ReadToEnd();
            MatchCollection matches = messageRegex.Matches(file);
            Dictionary<string, Dictionary<string, int>> firstWordCounts = new Dictionary<string, Dictionary<string, int>>();
            Dictionary<string, int> totalMessages = new Dictionary<string, int>();
            int currentIndex = -1;
            string currentName = null;

            foreach (Match m in matches)
            {
                if (currentIndex >= 0 && currentName != null)
                    ExtractMessage(file, currentIndex, m.Index - currentIndex, firstWordCounts, totalMessages, currentName);

                currentIndex = m.Index + m.Length;
                currentName = m.Groups[1].Value;
            }

            if (currentIndex >= 0 && currentName != null)
                ExtractMessage(file, currentIndex, file.Length - currentIndex, firstWordCounts, totalMessages, currentName);

            Directory.CreateDirectory(args[1]);
            HashSet<string> mostPopularWords = new HashSet<string>();
            foreach (KeyValuePair<string, Dictionary<string, int>> entry in firstWordCounts)
            {
                List<KeyValuePair<string, int>> sortedFirstWordCounts = new List<KeyValuePair<string, int>>(entry.Value);
                sortedFirstWordCounts.Sort((kvp1, kvp2) =>
                {
                    return kvp2.Value.CompareTo(kvp1.Value);
                });
                using (StreamWriter sw = new StreamWriter(Path.Combine(args[1], string.Format("{0}.csv", entry.Key))))
                {
                    int total = totalMessages[entry.Key];
                    for (int i = 0; i < Math.Min(WORDS_PER_PERSON, sortedFirstWordCounts.Count); i++)
                    {
                        KeyValuePair<string, int> kvp = sortedFirstWordCounts[i];
                        sw.WriteLine("{0}, {1}", kvp.Key, (double)kvp.Value / total);
                        mostPopularWords.Add(kvp.Key);
                    }
                }
            }

            List<string> names = new List<string>(firstWordCounts.Keys);
            names.Sort();

            Dictionary<string, int> mostPopularWordCounts = new Dictionary<string, int>();
            foreach (string word in mostPopularWords)
            {
                int total = 0;
                foreach(string name in names)
                {
                    if (!firstWordCounts[name].TryGetValue(word, out int count))
                        count = 0;
                    total += count;
                }
                mostPopularWordCounts[word] = total;
            }
            List<KeyValuePair<string, int>> sortedMostPopularWords = new List<KeyValuePair<string, int>>(mostPopularWordCounts);
            sortedMostPopularWords.Sort((kvp1, kvp2) =>
            {
                return kvp2.Value.CompareTo(kvp1.Value);
            });

            using (StreamWriter sw = new StreamWriter(Path.Combine(args[1], "Posterior.csv")))
            {
                sw.Write("Word");
                foreach (string name in names)
                    sw.Write(", {0}", name);
                sw.WriteLine();
                foreach (KeyValuePair<string, int> w in sortedMostPopularWords)
                {
                    sw.Write(w.Key);
                    foreach (string name in names)
                    {
                        if (!firstWordCounts[name].TryGetValue(w.Key, out int count))
                            count = 0;
                        sw.Write(", {0}", ((double)count / totalMessages[name]) * (double)totalMessages[name] / (double)w.Value);  //Bayes' theorem
                    }
                    sw.WriteLine();
                }
            }
        }
    }
}
